package com.ostapiuk.model.task5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CommentsReader {
    public static void main(String args[]) {
        displayComments(getEnteredFilename());
    }

    private static String getEnteredFilename(){
        Scanner fileName = new Scanner(System.in);
        System.out.println("Enter file name:");
        return fileName.nextLine();
    }

    private static void displayComments(String name) {
        String findComments;
        try (BufferedReader buffer = new BufferedReader(new FileReader(name))) {
            while (buffer.ready()) {
                findComments = buffer.readLine().trim();
                if (findComments.startsWith("//")) {
                    System.out.println(findComments);
                }
                if (findComments.startsWith("/*")) {
                    System.out.println(findComments);
                    while (!findComments.endsWith("*/")) {
                        findComments = buffer.readLine();
                        System.out.println(findComments);
                    }
                    System.out.println();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
