package com.ostapiuk.model.task4;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyInputStream extends FilterInputStream {
    private byte[] buffer;
    private int position;

    public MyInputStream(InputStream in, int size) {
        super(in);
        if (size <= 0) {
            throw new IllegalArgumentException("size <= 0");
        }
        this.buffer = new byte[size];
        this.position = size;
    }

    public MyInputStream(InputStream in) {
        this(in, 1);
    }

    private void ensureOpen() throws IOException {
        if (in == null) {
            throw new IOException("Stream closed");
        }
    }

    @Override
    public int read() throws IOException {
        ensureOpen();
        if (position < buffer.length) {
            return buffer[position++] & 0xff;
        }
        return super.read();
    }

    public int available() throws IOException {
        ensureOpen();
        int n = buffer.length - position;
        int avail = super.available();
        return n > (Integer.MAX_VALUE - avail)
                ? Integer.MAX_VALUE
                : n + avail;
    }

    public synchronized void close() throws IOException {
        if (in == null) {
            return;
        }
        in.close();
        in = null;
        buffer = null;
    }
}
