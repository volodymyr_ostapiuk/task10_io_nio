package com.ostapiuk.model.task7;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class SomeBuffer {
    private static FileChannel fileChannel;
    private static ByteBuffer buffer;

    public static void main(String[] args) throws IOException {
        readDataFromChannel(readPath());
        writeDataToChannel(readPath(), "Data into channel by Ostapiuk");
    }

    private static Path readPath() throws FileNotFoundException {
        return Paths.get("task7.txt");
    }

    private static void readDataFromChannel(Path path) throws IOException {
        fileChannel = FileChannel.open(Paths.get(String.valueOf(path)));
        buffer = ByteBuffer.allocate(1024);
        System.out.print("Read: ");
        while (fileChannel.read(buffer) != -1) {
            buffer.flip();
            while (buffer.hasRemaining()) {
                System.out.print((char) buffer.get());
            }
            buffer.clear();
        }
    }

    private static void writeDataToChannel(Path path, String message) throws IOException {
        fileChannel = FileChannel.open(Paths.get(
                String.valueOf(path)), StandardOpenOption.WRITE);
        buffer = ByteBuffer.wrap(message.getBytes());
        fileChannel.write(buffer);
        System.out.println("\nWrite: " + message);
    }
}
