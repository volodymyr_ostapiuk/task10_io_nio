package com.ostapiuk.model.task3;

import com.ostapiuk.model.task4.MyInputStream;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

public class CompareReading {
    private static FileInputStream fileInputStream;

    public static void main(String[] args) throws IOException {
        File file = new File("task3.txt");
        System.out.println("File size: " + file.length() / 1024 + " Kb");
        int[] size = new int[] {1, 2, 4, 8, 12, 16, 20};
        for (int value : size) {
            System.out.println("Buffered Reader of " + value
                    + " MB takes: " + findTimeBufferedReader(value) + " ms");
        }
        System.out.println("Usual reading takes: " + findTimeUsualReader() + " ms");
        System.out.println("Reader with MyInputStream of 16 MB takes: "
                + findTimeMyReader() + " ms");
    }

    private static long findTimeUsualReader() throws IOException {
        fileInputStream = new FileInputStream("task3.txt");
        Date dateStart = new Date();
        while (fileInputStream.available() > 0) {
            fileInputStream.read();
        }
        Date dateEnd = new Date();
        fileInputStream.close();
        return dateEnd.getTime() - dateStart.getTime();
    }

    private static long findTimeBufferedReader(int value) throws IOException {
        fileInputStream = new FileInputStream("task3.txt");
        BufferedInputStream bis = new BufferedInputStream(fileInputStream, value * 1024);
        Date dateStart = new Date();
        while (bis.available() > 0) {
            bis.read(new byte[1024]);
        }
        Date dateEnd = new Date();
        fileInputStream.close();
        bis.close();
        return dateEnd.getTime() - dateStart.getTime();
    }

    private static long findTimeMyReader() throws IOException {
        fileInputStream = new FileInputStream("task3.txt");
        MyInputStream myInputStream = new MyInputStream(fileInputStream, 16 * 1024);
        Date dateStart = new Date();
        while (myInputStream.available() > 0) {
            myInputStream.read(new byte[1024]);
        }
        Date dateEnd = new Date();
        fileInputStream.close();
        myInputStream.close();
        return dateEnd.getTime() - dateStart.getTime();
    }
}
