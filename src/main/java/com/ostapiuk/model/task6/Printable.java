package com.ostapiuk.model.task6;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

    void print() throws IOException;
}
